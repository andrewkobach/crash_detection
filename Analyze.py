from ParseAccelSignal import get_accel_signal
import glob
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os

#sde = r".\PFFCB182202110122057410002HDS.SDE"

parse = argparse.ArgumentParser()
parse.add_argument("--makeplots", action='store_true', default=False, help="produce PDF of signals")
parse.add_argument("--benchmark", action='store_true', default=False, help="run benchmark analysis")
args = parse.parse_args()



def abs_accel(a):
    (ax05, ay05, az05), (ax5, ay5, az5) = a
    a_mag = [(x**2 + y**2 + (z-1.)**2)**0.5 for x,y,z in zip(ax5,ay5,az5)]
    a_mag_max = max(a_mag)
    
    return a_mag_max, a_mag

    
    
if __name__ == "__main__":    

    # get list of SDE files
    crash_SDEs = glob.glob("./data/crash_events/*.SDE")
    non_crash_SDEs = glob.glob("./data/non_crash_events/*.SDE")
    
    
    # MAKE PLOTS
    if args.makeplots:
        #SDEs = [r".\PFFCB182202110122057410002HDS.SDE"]
        SDEs = glob.glob("./data/*/*.SDE")
        
        for sde in SDEs:
            try:
                (ax05, ay05, az05), (ax5, ay5, az5) = get_accel_signal(sde)
            except: 
                continue
            
            plt.rcParams["figure.figsize"] = (20,10)
            fig, axs = plt.subplots(3,2)
            t_lowg = np.linspace(0,20,num=len(ax5))
            ypad = 1.5
            
            axs[0,0].plot(t_lowg, ax5, color='b', label=r'$a_x~[g]$, 5 Hz')
            axs[0,0].plot(t_lowg, ax05, color='k', label=r'$a_x~[g]$, 0.5 Hz')
            axs[0,0].set_title(r'low-g accelerometer')
            axs[0,0].legend(loc='upper left')
            axs[0,0].set_ylim(-ypad, ypad)
            
            axs[1,0].plot(t_lowg, ay5, color='g', label=r'$a_y~[g]$, 5 Hz')
            axs[1,0].plot(t_lowg, ay05, color='k', label=r'$a_y~[g]$, 0.5 Hz')
            axs[1,0].legend(loc='upper left')
            axs[1,0].set_ylim(-ypad, ypad)
            
            axs[2,0].plot(t_lowg, az5, color='r', label=r'$a_z ~[g]$, 5 Hz')
            axs[2,0].plot(t_lowg, az05, color='k', label=r'$a_z ~[g]$, 0.5 Hz')
            axs[2,0].set_xlabel('time [s]')
            axs[2,0].legend(loc='upper left')
            axs[2,0].set_ylim(1-ypad, 1+ypad)
            
            # subtract out 0.5 Hz signal
            ax_sub = [a-b for a,b in zip(ax5,ax05)]
            ay_sub = [a-b for a,b in zip(ay5,ay05)]
            az_sub = [a-b for a,b in zip(az5,az05)]
            
            axs[0,1].plot(t_lowg, ax_sub, color='dodgerblue', label=r'$a_x~[g]$, 5 Hz - 0.5 Hz')
            axs[0,1].set_title(r'5 Hz signal with 0.5 signal subtracted out')
            axs[0,1].legend(loc='upper left')
            axs[0,1].set_ylim(-ypad, ypad)
            
            axs[1,1].plot(t_lowg, ay_sub, color='limegreen', label=r'$a_y~[g]$, 5 Hz - 0.5 Hz')
            axs[1,1].legend(loc='upper left')
            axs[1,1].set_ylim(-ypad, ypad)
            
            axs[2,1].plot(t_lowg, az_sub, color='tomato', label=r'$a_z~[g]$, 5 Hz - 0.5 Hz')
            axs[2,1].legend(loc='upper left')
            axs[2,1].set_ylim(-ypad, ypad)
            
           
            plotpath = os.path.dirname(sde)
            plotname = sde.split(os.sep)[-1] + "_signal.png"
            plt.savefig(os.path.join(plotpath, plotname))
            plt.close()
        
        exit()
        
    
    
    
    # BASELINE ANALYSIS
    if args.benchmark:
        thresholds = np.linspace(0.1, 5, 50)
        false_positive_rate = []
        false_negative_rate = []
        
        for threshold in thresholds:
            cnt_crashes = 0
            cnt_non_crashes = 0
            cnt_crash_detected_in_crash_event = 0
            cnt_crash_detected_in_non_crash_event = 0
        
            for filename in crash_SDEs:
                # some safety.bin files are corrupt
                try:
                    a_mag_max, a_mag = abs_accel(get_accel_signal(filename))
                    cnt_crashes += 1
                    if a_mag_max >= threshold:
                        cnt_crash_detected_in_crash_event += 1 
                except:
                    pass
            
            
            for filename in non_crash_SDEs:
                # some safety.bin files are corrupt
                try:
                    a_mag_max, a_mag = abs_accel(get_accel_signal(filename))
                    cnt_non_crashes += 1
                    if a_mag_max >= threshold:
                        cnt_crash_detected_in_non_crash_event += 1 
                except:
                    pass
            
            print(f"Threshold for magnitude of acceleration: {threshold} g")
            print(f"Total number of crashes detected / total number of crash events: {cnt_crash_detected_in_crash_event} / {cnt_crashes}")
            print(f"Total number of crashes detected / total number of non crash events: {cnt_crash_detected_in_non_crash_event} / {cnt_non_crashes}")
            print(f"False positive rate: {cnt_crash_detected_in_non_crash_event/cnt_non_crashes}")
            print(f"False negative rate: {1- cnt_crash_detected_in_crash_event/cnt_crashes}")
            
            false_positive_rate.append(cnt_crash_detected_in_non_crash_event/cnt_non_crashes)
            false_negative_rate.append(1- cnt_crash_detected_in_crash_event/cnt_crashes)
        
        
        
        plt.plot(false_positive_rate, false_negative_rate, "ko", label=r'Varying threshold on $|\vec{a}|$')
        plt.xlabel("False positive rate [% triggered on non-crash]")
        plt.ylabel("False negative rate [% missed crashes]")
        plt.xlim(0,1)
        plt.ylim(0,1)
        plt.legend(loc='upper right')
        plt.show()
        exit()














