from ParseAccelSignal import get_accel_signal, get_accel_signal_fast
import glob
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import pandas as pd
from bioinfokit.analys import get_data
from bioinfokit.visuz import cluster
from sklearn.manifold import TSNE
import statistics
from tqdm import tqdm

#sde = r".\PFFCB182202110122057410002HDS.SDE"

parse = argparse.ArgumentParser()
parse.add_argument("--makeplots", action='store_true', default=False, help="produce PDF of signals")
parse.add_argument("--benchmark", action='store_true', default=False, help="run benchmark analysis")
parse.add_argument("--makedata", action='store_true', default=False, help="make data set from SDE files")
parse.add_argument("--tsne", action='store_true', default=False, help="analyze data with TSNE")
parse.add_argument("--scatter", action='store_true', default=False, help="analyze data with TSNE")
parse.add_argument("--flux", action='store_true', default=False, help="analyze total flux of data")
parse.add_argument("--test", action='store_true', default=False, help="sandbox mode")
args = parse.parse_args()



def abs_accel(ax, ay, az, mode='do_not_subtract_gravity'):
    #ax5 = data["5Hz_ax"]
    # = data["5Hz_ay"]
    #az5 = data["5Hz_az"]
    
    if mode == 'subtract_gravity':
        return [(x**2 + y**2 + (z-1)**2)**0.5 for x,y,z in zip(ax,ay,az)]
    
    else:
        return [(x**2 + y**2 + z**2)**0.5 for x,y,z in zip(ax,ay,az)]
    


def Integrate(a, epsilon=1, const=1):
    
    A = []
    
    s = 0
    for ai in a:
        s += ai
        A.append(s*epsilon*const)
        
    return A


def rolling_average(a, nsamples):
    
    # number of samples in original signal
    n = len(a)
    
    # define edges of chunks over which we'll average
    endpoints = [int(x) for x in np.linspace(0, n-1, num=nsamples+1)]
    
    # calculate average for each chunk
    a_smoothed = []
    for i in range(len(endpoints)-1):
        
        #print(i,endpoints[i], endpoints[i+1], a[endpoints[i]], a[endpoints[i+1]])
    
        a_smoothed.append(statistics.mean(a[endpoints[i]:endpoints[i+1]]))
    
    return a_smoothed




def get_features(data):
        
    # make fine acceleration signal
    ax_gross = data["0.5Hz_ax"]
    ay_gross = data["0.5Hz_ay"]
    az_gross = data["0.5Hz_az"]
    ax_fine = [a-b for a,b in zip(data["5Hz_ax"], data["0.5Hz_ax"])]
    ay_fine = [a-b for a,b in zip(data["5Hz_ay"], data["0.5Hz_ay"])]
    az_fine = [a-b for a,b in zip(data["5Hz_az"], data["0.5Hz_az"])]
    abs_fine_accel = abs_accel(ax_fine, ay_fine, az_fine)
    
    # define index locations of window endpoints 
    indices_per_second = data["length"]/data["duration"]
    impulse_window_width = 2*indices_per_second # number of indices for 2 seconds
    leadup_window_width = 2*indices_per_second
    ringdown_window_width = 2*indices_per_second
    peak_index = abs_fine_accel.index(max(abs_fine_accel))
    impulse_window_endpoints = [int(peak_index - impulse_window_width/2), int(peak_index + impulse_window_width/2)]
    leadup_window_endpoints = [int(peak_index - impulse_window_width/2 - leadup_window_width), int(peak_index - impulse_window_width/2)]
    ringdown_window_endpoints = [int(peak_index + impulse_window_width/2), int(peak_index + impulse_window_width/2 + ringdown_window_width)]
    
    if leadup_window_endpoints[0] < 0 or ringdown_window_endpoints[1] > data["length"]-1:
        return {}
        
    
    # maximal value of fine acceleration signal in inpulse window
    ax_fine_impulse = ax_fine[impulse_window_endpoints[0]:impulse_window_endpoints[1]]
    ay_fine_impulse = ay_fine[impulse_window_endpoints[0]:impulse_window_endpoints[1]]
    az_fine_impulse = az_fine[impulse_window_endpoints[0]:impulse_window_endpoints[1]]
    peak_fine_x = abs(max(ax_fine_impulse, key=abs))
    peak_fine_y = abs(max(ay_fine_impulse, key=abs))
    peak_fine_z = abs(max(az_fine_impulse, key=abs))
    
    # average of absolute values of fine acceleration in lead up window
    ax_fine_leadup = ax_fine[leadup_window_endpoints[0]:leadup_window_endpoints[1]]
    ay_fine_leadup = ay_fine[leadup_window_endpoints[0]:leadup_window_endpoints[1]]
    az_fine_leadup = az_fine[leadup_window_endpoints[0]:leadup_window_endpoints[1]]
    avg_fine_x_leadup = sum([abs(x) for x in ax_fine_leadup])/len(ax_fine_leadup)
    avg_fine_y_leadup = sum([abs(y) for y in ay_fine_leadup])/len(ay_fine_leadup)
    avg_fine_z_leadup = sum([abs(z) for z in az_fine_leadup])/len(az_fine_leadup)
    
    # average abs of gross ax in lead up and ring down windows window
    ax_gross_leadup = ax_gross[leadup_window_endpoints[0]:leadup_window_endpoints[1]]
    ax_gross_ringdown = ax_gross[ringdown_window_endpoints[0]:ringdown_window_endpoints[1]]
    avg_ax_gross_leadup = sum(ax_gross_leadup)/len(ax_gross_leadup)
    avg_ax_gross_ringdown = sum(ax_gross_ringdown)/len(ax_gross_ringdown)
    
    # peak values of gross ay over all windows
    ay_gross_allwindows = ay_gross[leadup_window_endpoints[0]:ringdown_window_endpoints[1]]
    max_ay_gross_allwindows = max(ay_gross_allwindows)
    min_ay_gross_allwindows = min(ay_gross_allwindows)

    
    # fill dictionary
    features = {}
    features["peak_fine_x_impulse"] = peak_fine_x
    features["peak_fine_y_impulse"] = peak_fine_y
    features["peak_fine_z_impulse"] = peak_fine_z
    features["avg_fine_x_leadup"] = avg_fine_x_leadup
    features["avg_fine_y_leadup"] = avg_fine_y_leadup
    features["avg_fine_z_leadup"] = avg_fine_z_leadup
    features["avg_gross_x_leadup"] = avg_ax_gross_leadup
    features["avg_gross_x_ringdown"] = avg_ax_gross_ringdown
    features["max_ay_gross_allwindows"] = max_ay_gross_allwindows
    features["min_ay_gross_allwindows"] = min_ay_gross_allwindows
    
    
    
    return features

    
    
if __name__ == "__main__":    
    
    
    if args.test:
        
        df_crash = pd.read_pickle("./data/crash_events.pkl")
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            print(df_crash[["trigger_type_name", "trigger_type_id", "sde_file_name"]])
    
    
    
    if args.flux:
        
        list_crash = pd.read_pickle("./data/crash_events_with_acc.pkl")
        list_all = pd.read_pickle("./data/all_events_with_acc.pkl")
        
        #print(list_crash[0].keys())
                
        thresholds = np.linspace(0,2.5,20)
        true_positive_rate = []
        num_prioritized_events = []
        
        for threshold in thresholds:
            
            # crash events
            num_crash_events = 0
            num_crash_events_detected = 0
            for event in list_crash:
                if event['corrupt']:
                    continue
                a_mag_max = max(abs_accel(event["5Hz_ax"], event["5Hz_ay"], event["5Hz_az"],'subtract_gravity'))
                num_crash_events += 1
                if a_mag_max >= threshold:
                    num_crash_events_detected += 1
                
            true_positive_rate.append(num_crash_events_detected/num_crash_events)
            
            
            
            # all events
            num_all_events = 0
            num_all_events_detected = 0
            for event in list_all:
                if event['corrupt']:
                    continue
                a_mag_max = max(abs_accel(event["5Hz_ax"], event["5Hz_ay"], event["5Hz_az"],'subtract_gravity'))
                num_all_events += 1
                if a_mag_max >= threshold:
                    num_all_events_detected += 1
              
            num_prioritized_events.append(num_all_events_detected/num_all_events)
            
        
            print(f"\nThreshold = {threshold} g")
            print(f"number crashes detected in crash events / number of crashes = {num_crash_events_detected}/{num_crash_events}")
            print(f"number crashes detected in all events / number of all events = {num_all_events_detected}/{num_all_events}")
            
            #num_HDS_total = len(df_all[df_all['trigger_type_id'].isin([345, 365])].index)
            #num_total = len(df_all.index)
            #num_HDS_crashes = len(df_crash[df_crash['trigger_type_id'].isin([345, 365])].index)
            #num_crashes = len(df_crash.index)
        
        plt.plot(num_prioritized_events, true_positive_rate, "ko", label=r'Varying threshold on $|\vec{a}|$')
        #plt.plot([num_HDS_total/num_total], [num_HDS_crashes/num_crashes], marker="s", color="purple", linestyle='None', label="HD Shock")
        plt.xlabel("fraction of all auto-offloaded events that are prioritized")
        plt.ylabel("% auto-offloaded crashes detected")
        plt.xlim(0,1)
        plt.ylim(0,1)
        plt.legend(loc='lower right')
        plt.show()
    
    
    '''
    if args.flux:
        
        df_crash = pd.read_pickle("./data/crash_events.pkl")
        df_all = pd.read_pickle("./data/all_events.pkl")
        
        print("fraction of HD shock events: ", len(df_all[df_all['trigger_type_id'].isin([345, 365])].index)/len(df_all.index) )
        
        # downsample
        total_number_of_events = len(df_all.index)
        df_all = df_all.sample(n=1000)
        
        print("fraction of HD shock events after down sampling: ", len(df_all[df_all['trigger_type_id'].isin([345, 365])].index)/len(df_all.index) )


        
        crash_filenames = df_crash['sde_file_name'].values.tolist()
        all_filenames = df_all['sde_file_name'].values.tolist()
    
        
        thresholds = np.linspace(0,2.5,20)
        true_positive_rate = []
        num_prioritized_events = []
        
        for threshold in thresholds:
        
            # crash events
            num_crash_events = 0
            num_crash_events_detected = 0
            for sde in tqdm(crash_filenames):
                sr = sde[:8]
                src = os.path.join(r"\\Fas01-b\epp", sr, sde)
                try:
                    data = get_accel_signal_fast(src)
                    if data['corrupt']:
                        continue
                    a_mag_max = max(abs_accel(data["5Hz_ax"], data["5Hz_ay"], data["5Hz_az"],'subtract_gravity'))
                    num_crash_events += 1
                    if a_mag_max >= threshold:
                        num_crash_events_detected += 1
                except:
                    pass
            true_positive_rate.append(num_crash_events_detected/num_crash_events)
                
            # all events
            num_all_events = 0
            num_all_events_detected = 0
            for sde in tqdm(all_filenames):
                sr = sde[:8]
                src = os.path.join(r"\\Fas01-b\epp", sr, sde)
                try:
                    data = get_accel_signal_fast(src)
                    if data['corrupt']:
                        continue
                    a_mag_max = max(abs_accel(data["5Hz_ax"], data["5Hz_ay"], data["5Hz_az"],'subtract_gravity'))
                    num_all_events += 1
                    if a_mag_max >= threshold:
                        num_all_events_detected += 1
                except:
                    pass
            num_prioritized_events.append(num_all_events_detected/num_all_events)
                    
            print(f"\nThreshold = {threshold} g")
            print(f"number crashes detected in crash events / number of crashes = {num_crash_events_detected}/{num_crash_events}")
            print(f"number crashes detected in all events / number of all events = {num_all_events_detected}/{num_all_events}")
            
            num_HDS_total = len(df_all[df_all['trigger_type_id'].isin([345, 365])].index)
            num_total = len(df_all.index)
            num_HDS_crashes = len(df_crash[df_crash['trigger_type_id'].isin([345, 365])].index)
            num_crashes = len(df_crash.index)
        
        plt.plot(num_prioritized_events, true_positive_rate, "ko", label=r'Varying threshold on $|\vec{a}|$')
        plt.plot([num_HDS_total/num_total], [num_HDS_crashes/num_crashes], marker="s", color="purple", linestyle='None', label="HD Shock")
        plt.xlabel("fraction of all auto-offloaded events that are prioritized")
        plt.ylabel("% auto-offloaded crashes detected")
        plt.xlim(0,1)
        plt.ylim(0,1)
        plt.legend(loc='lower right')
        plt.show()
        '''
    
    
    

    # get list of SDE files
    crash_SDEs = glob.glob("./data/crash_events/*.SDE")
    non_crash_SDEs = glob.glob("./data/non_crash_events/*.SDE")
    
    
    
    # MAKE PLOTS
    if args.makeplots:
        #SDEs = [r".\PFFCB182202110122057410002HDS.SDE"]
        #SDEs = [r".\QDHCB109202112301525470025HDS.SDE"]
        #SDEs = [r".\QAXCB094202201241026510008HDS.SDE"]
        SDEs = glob.glob("./data/*/*.SDE")
        
        for sde in SDEs:
            
            data = get_accel_signal(sde)
            
            if data["corrupt"]:
                continue
                
            plt.rcParams["figure.figsize"] = (20,10)
            fig, axs = plt.subplots(4,2)
            t_lowg = np.linspace(0,20,num=data["length"])
            t_highg = np.linspace(0,20,num=len(data["ax_highg"]))
            ypad = 1.5
            impulse_width = 2
            leadup_width = 2
            ringdown_width = 2
            
            # find impulse
            ax_sub = [a-b for a,b in zip(data["5Hz_ax"], data["0.5Hz_ax"])]
            ay_sub = [a-b for a,b in zip(data["5Hz_ay"], data["0.5Hz_ay"])]
            az_sub = [a-b for a,b in zip(data["5Hz_az"], data["0.5Hz_az"])]
            a_mag = abs_accel(ax_sub,ay_sub,az_sub)
            impulse_index = a_mag.index(max(a_mag))
            impulse_time = data["duration"]*impulse_index/data["length"]
            
            impulse_start_time = impulse_time-impulse_width/2
            impulse_end_time = impulse_time+impulse_width/2
            leadup_start_time = impulse_start_time - leadup_width
            leadup_end_time = impulse_start_time
            ringdown_start_time = impulse_end_time
            rigndown_end_time = ringdown_start_time + ringdown_width
            
            
            axs[0,0].plot(t_lowg, data["5Hz_ax"], color='b', label=r'$a_x~[g]$, 5 Hz')
            axs[0,0].plot(t_lowg, data["0.5Hz_ax"], color='k', label=r'$a_x~[g]$, 0.5 Hz')
            axs[0,0].axvspan(impulse_start_time, impulse_end_time, alpha=0.5, color='red')
            axs[0,0].axvspan(leadup_start_time, leadup_end_time, alpha=0.25, color='red')
            axs[0,0].axvspan(ringdown_start_time, rigndown_end_time, alpha=0.25, color='red')
            axs[0,0].set_title(r'low-g accelerometer')
            axs[0,0].legend(loc='upper left')
            axs[0,0].set_ylim(-ypad, ypad)
            
            axs[1,0].plot(t_lowg, data["5Hz_ay"], color='g', label=r'$a_y~[g]$, 5 Hz')
            axs[1,0].plot(t_lowg, data["0.5Hz_ay"], color='k', label=r'$a_y~[g]$, 0.5 Hz')
            axs[1,0].axvspan(impulse_start_time, impulse_end_time, alpha=0.5, color='red')
            axs[1,0].axvspan(leadup_start_time, leadup_end_time, alpha=0.25, color='red')
            axs[1,0].axvspan(ringdown_start_time, rigndown_end_time, alpha=0.25, color='red')
            axs[1,0].legend(loc='upper left')
            axs[1,0].set_ylim(-ypad, ypad)
            
            axs[2,0].plot(t_lowg, data["5Hz_az"], color='r', label=r'$a_z ~[g]$, 5 Hz')
            axs[2,0].plot(t_lowg, data["0.5Hz_az"], color='k', label=r'$a_z ~[g]$, 0.5 Hz')
            axs[2,0].axvspan(impulse_start_time, impulse_end_time, alpha=0.5, color='red')
            axs[2,0].axvspan(leadup_start_time, leadup_end_time, alpha=0.25, color='red')
            axs[2,0].axvspan(ringdown_start_time, rigndown_end_time, alpha=0.25, color='red')
            axs[2,0].legend(loc='upper left')
            axs[2,0].set_ylim(1-ypad, 1+ypad)
            
            #speed_from_accel_x = [x+data["speed"][0] for x in Integrate(data["5Hz_ax"], 1/20, 9.8*1/1000*3600)]
            #speed_from_accel_y = [x+data["speed"][0] for x in Integrate(data["5Hz_ay"], 1/20, 9.8*1/1000*3600)]
            axs[3,0].plot(t_lowg, data["speed"], color='purple', label=r'speed [kph]')
            #axs[3,0].plot(t_lowg, speed_from_accel_x, color='b', label=r'speed from $a_x$ at 0.5 Hz [kph]')
            #axs[3,0].plot(t_lowg, speed_from_accel_y, color='g', label=r'speed from $a_y$ at 0.5 Hz [kph]')
            axs[3,0].legend(loc='upper left')
            axs[3,0].set_xlabel('time [s]')
            
            '''
            axs[0,1].plot(t_lowg, ax_sub, color='blue', label=r'$a_x~[g]$, 5 Hz - 0.5 Hz')
            axs[0,1].axvspan(impulse_start_time, impulse_end_time, alpha=0.5, color='red')
            axs[0,1].axvspan(leadup_start_time, leadup_end_time, alpha=0.25, color='red')
            axs[0,1].axvspan(ringdown_start_time, rigndown_end_time, alpha=0.25, color='red')
            axs[0,1].set_title(r'5 Hz signal with 0.5 signal subtracted out')
            axs[0,1].legend(loc='upper left')
            axs[0,1].set_ylim(-ypad, ypad)
            
            axs[1,1].plot(t_lowg, ay_sub, color='green', label=r'$a_y~[g]$, 5 Hz - 0.5 Hz')
            axs[1,1].axvspan(impulse_start_time, impulse_end_time, alpha=0.5, color='red')
            axs[1,1].axvspan(leadup_start_time, leadup_end_time, alpha=0.25, color='red')
            axs[1,1].axvspan(ringdown_start_time, rigndown_end_time, alpha=0.25, color='red')
            axs[1,1].legend(loc='upper left')
            axs[1,1].set_ylim(-ypad, ypad)
            
            axs[2,1].plot(t_lowg, az_sub, color='red', label=r'$a_z~[g]$, 5 Hz - 0.5 Hz')
            axs[2,1].axvspan(impulse_start_time, impulse_end_time, alpha=0.5, color='red')
            axs[2,1].axvspan(leadup_start_time, leadup_end_time, alpha=0.25, color='red')
            axs[2,1].axvspan(ringdown_start_time, rigndown_end_time, alpha=0.25, color='red')
            axs[2,1].legend(loc='upper left')
            axs[2,1].set_ylim(-ypad, ypad)
            '''
            
            if len(t_highg) > 0:
                highgpad = 200
                
                abshighg = abs_accel(data["ax_highg"], data["ay_highg"], data["az_highg"])
                highgpeak = abshighg.index(max(abshighg))
                
                axs[0,1].plot(t_highg[highgpeak-highgpad:highgpeak+highgpad], data["ax_highg"][highgpeak-highgpad:highgpeak+highgpad], color='blue', label=r'$a_x~[g]$, high-g')
                axs[0,1].legend(loc='upper left')
                
                axs[1,1].plot(t_highg[highgpeak-highgpad:highgpeak+highgpad], data["ay_highg"][highgpeak-highgpad:highgpeak+highgpad], color='green', label=r'$a_y~[g]$, high-g')
                axs[1,1].legend(loc='upper left')
                
                axs[2,1].plot(t_highg[highgpeak-highgpad:highgpeak+highgpad], data["az_highg"][highgpeak-highgpad:highgpeak+highgpad], color='red', label=r'$a_z~[g]$, high-g')
                axs[2,1].legend(loc='upper left')
            
            axs[3,1].plot(t_lowg, a_mag, color='k', label=r'$|\vec{a}_{sub}|$ [g], low g')
            axs[3,1].axvspan(impulse_start_time, impulse_end_time, alpha=0.5, color='red')
            axs[3,1].axvspan(leadup_start_time, leadup_end_time, alpha=0.25, color='red')
            axs[3,1].axvspan(ringdown_start_time, rigndown_end_time, alpha=0.25, color='red')
            axs[3,1].legend(loc='upper left')
            axs[3,1].set_ylim(0, ypad)
            
           
            plotpath = os.path.dirname(sde)
            plotname = sde.split(os.sep)[-1] + "_signal.png"
            plt.savefig(os.path.join(plotpath, plotname))
            plt.close()
        
        exit()
        
    
    
    
    # MAKE DATA
    if(args.makedata):
        
        #SDEs = [r".\PFFCB182202110122057410002HDS.SDE"]
        #SDEs = [r".\QDHCB109202112301525470025HDS.SDE"]
        SDEs = glob.glob("./data/*/*.SDE")
        
        events = []
        
        for sde in SDEs:
            
            data = get_accel_signal(sde)
            
            if data["corrupt"]:
                continue
            
            features = get_features(data)
            
            if not bool(features):
                continue
            
            if sde.split(os.sep)[-2] == 'crash_events':
                features['collision'] = 1
            if sde.split(os.sep)[-2] == 'non_crash_events':
                features['collision'] = 0
                
            events.append(features)
    
            
        df = pd.DataFrame(events)
        
        df.to_csv("event_features.csv", index=False)
        
        
        
        if args.tsne:
            tsne_em = TSNE(n_components=2, perplexity=50.0, n_iter=1000, verbose=1).fit_transform(df.drop(columns=["collision"]))
            cluster.tsneplot(score=tsne_em)
            color_class = df["collision"].to_numpy()
            cluster.tsneplot(score=tsne_em, colorlist=color_class, legendpos='upper right', legendanchor=(1.15, 1) )
                        
    
    
    
    # BENCHMARK ANALYSIS
    if args.benchmark:
        thresholds = np.linspace(0.0, 2.5, 25)
        false_positive_rate = []
        false_negative_rate = []
        
        for threshold in thresholds:
            cnt_crashes = 0
            cnt_non_crashes = 0
            cnt_crash_detected_in_crash_event = 0
            cnt_crash_detected_in_non_crash_event = 0
        
            for filename in crash_SDEs:
                # some safety.bin files are corrupt
                try:
                    data = get_accel_signal(filename, mode='do_not_parse_highg')
                    a_mag_max = max(abs_accel(data["5Hz_ax"], data["5Hz_ay"], data["5Hz_az"],'subtract_gravity'))
                    '''
                    ax_sub = [a-b for a,b in zip(data["5Hz_ax"], data["0.5Hz_ax"])]
                    ay_sub = [a-b for a,b in zip(data["5Hz_ay"], data["0.5Hz_ay"])]
                    az_sub = [a-b for a,b in zip(data["5Hz_az"], data["0.5Hz_az"])]
                    a_mag_max = a_mag_max = max(abs_accel(ax_sub, ay_sub, az_sub,'do_not_subtract_gravity'))
                    '''
                    cnt_crashes += 1
                    if a_mag_max >= threshold:
                        cnt_crash_detected_in_crash_event += 1 
                except:
                    pass
            
            
            for filename in non_crash_SDEs:
                # some safety.bin files are corrupt
                try:
                    data = get_accel_signal(filename)
                    a_mag_max = max(abs_accel(data["5Hz_ax"], data["5Hz_ay"], data["5Hz_az"],'subtract_gravity'))
                    '''
                    ax_sub = [a-b for a,b in zip(data["5Hz_ax"], data["0.5Hz_ax"])]
                    ay_sub = [a-b for a,b in zip(data["5Hz_ay"], data["0.5Hz_ay"])]
                    az_sub = [a-b for a,b in zip(data["5Hz_az"], data["0.5Hz_az"])]
                    a_mag_max = a_mag_max = max(abs_accel(ax_sub, ay_sub, az_sub,'do_not_subtract_gravity'))
                    '''
                    cnt_non_crashes += 1
                    if a_mag_max >= threshold:
                        cnt_crash_detected_in_non_crash_event += 1 
                except:
                    pass
            
            print(f"Threshold for magnitude of acceleration: {threshold} g")
            print(f"Total number of crashes detected / total number of crash events: {cnt_crash_detected_in_crash_event} / {cnt_crashes}")
            print(f"Total number of crashes detected / total number of non crash events: {cnt_crash_detected_in_non_crash_event} / {cnt_non_crashes}")
            print(f"False positive rate: {cnt_crash_detected_in_non_crash_event/cnt_non_crashes}")
            print(f"False negative rate: {1- cnt_crash_detected_in_crash_event/cnt_crashes}")
            
            false_positive_rate.append(cnt_crash_detected_in_non_crash_event/cnt_non_crashes)
            false_negative_rate.append(1- cnt_crash_detected_in_crash_event/cnt_crashes)
        
        
        
        plt.plot(false_positive_rate, false_negative_rate, "ko", label=r'Varying threshold on $|\vec{a}|$')
        plt.xlabel("% triggered on non-crash")
        plt.ylabel("% missed crashes")
        plt.xlim(0,1)
        plt.ylim(0,1)
        plt.legend(loc='upper right')
        plt.show()
        exit()














