#include <iostream>
#include "crash_detection_funcs.h"

using namespace std;


// allocate memory for 3 components of acceleration
static float ax_buffer[length];
static float ay_buffer[length];
static float az_buffer[length]; 

// initialize instanteous values of signal, assign bogus values
static float ax = init_val;
static float ay = init_val;
static float az = init_val;

static const int crash_detection_frequency = 20; // after how many new elements does the buffer get evaulated by the machine-learning model
static int signal_cnt = 0; // counter
static float crash_probability = 0; // value between 0 and 1 that is returned by the machine-learning model 



int main(){ // pretending this is some external process that has access to the accelerometer signal readings at 20 Hz
    
    // start up - initialize arrays with obvious bogus values of acceleration
    init_array(ax_buffer);
    init_array(ay_buffer);
    init_array(az_buffer);

    // fill buffers with rolling values, this loop simulates live data
    for(int i=0; i<400; i++){ // fake loop over test data defined in header file, pretending to loop at 20 Hz
        ax = get_accel_component(i, 0); // pretending to be the function that gets the instantaneous accelerometer signal 
        ay = get_accel_component(i, 1);
        az = get_accel_component(i, 2);

        // fill buffers with rolling values
        // newest values to right-end of array, deletes first element
        add_to_buffer(ax_buffer, ax);
        add_to_buffer(ay_buffer, ay);
        add_to_buffer(az_buffer, az);

        // add one to counter
        signal_cnt++;

        // evalulate model every counter equals desired frequency
        if(signal_cnt >= crash_detection_frequency){ 
            crash_probability = get_crash_probability(ax_buffer, ay_buffer, az_buffer);  // send buffers to machine-learning model, return probability
            cout<<crash_probability<<endl;
            signal_cnt = 0; // set counter to 0
        }

    }


    return 0;

}