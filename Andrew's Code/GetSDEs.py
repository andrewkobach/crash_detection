"""
This script:
    queries the external data warehouse for crash and non-crash events
    creates a pandas dataframe with result of query
    copies SDE files to local directory

usage: GetSDEs.py [-h] [--n N]

optional arguments:
  -h, --help  show this help message and exit
  --n N       Number of SDE files for each directory

author: Andrew Kobach
"""

import argparse
from pathlib import Path
import pandas as pd
import pyodbc
import os
import shutil
from ParseAccelSignal import get_accel_signal, get_accel_signal_fast


# commmand line arguments
parse = argparse.ArgumentParser()
parse.add_argument("--n", type=int, default=100, help="Number of SDE files for each directory")
parse.add_argument("--copyfiles", action='store_true', default=False, help="Copy files to ./data")
parse.add_argument("--filelist", action='store_true', default=False, help="Save pkl files to ./data")
args = parse.parse_args()

number_of_samples = args.n

def get_crash_events():
    query = """Select 
    sr.serial_number As smartrecorder_sn, 
    sr.application_version, 
    v.serial_number As vehicle_sn, 
    s.company_name, 
    s.site_level_five_name, 
    e.event_number, 
    e.event_id,
    e.[file_name] as sde_file_name,
    LEFT(e.[file_name],len(e.[file_name])-4) as base_file_name,
    fel.captured_datetime, 
    tt.trigger_type_id, 
    tt.trigger_type_name, 
    o.observation_id, 
    o.observation_name, 
    o.category, 
    o.sub_category, 
    v.vehicle_id 
    From 
    sds_ExternalDataWarehouse.dbo.fact_Event_Lifecycle fel with (nolock)
    Join dbo.dim_SmartRecorder sr with (nolock) On fel.dim_smartrecorder_key = sr.dim_smartrecorder_key 
    Join dbo.dim_Trigger_Type tt with (nolock) On fel.dim_trigger_type_key = tt.dim_trigger_type_key 
    Join dbo.dim_Site s with (nolock) On sr.dim_sitehierarchy_key = s.dim_sitehierarchy_key 
    Join dbo.dim_Event e with (nolock) On fel.dim_event_key = e.dim_event_key 
    Join dbo.dim_Vehicle v with (nolock) On v.dim_vehicle_key = fel.dim_vehicle_key 
    Left Join dbo.fact_Event_Lifecycle_Observation felo with (nolock) On felo.dim_event_key = fel.dim_event_key 
    Left Join dbo.dim_Observation o with (nolock) On felo.dim_Observation_key = o.dim_observation_key 

    Where fel.captured_datetime < DATEADD(day,-14, GETDATE()) and fel.captured_datetime >= DATEADD(day,-28, GETDATE())
    and sr.application_version like '4%'
    and o.sub_category = 'Collision'


    order by newid()"""

    # specify the driver, server, and database
    driver = 'ODBC DRIVER 17 for SQL Server'
    server = 'db_externaldw.smartdrivesystems.com'
    database = 'sds_ExternalDataWarehouse'

    # choose a read-only node
    odbc = 'DRIVER={%s};SERVER=%s;DATABASE=%s;Trusted_Connection=Yes;ApplicationIntent=ReadOnly' % (
    driver, server, database)

    # connect to the database
    sql_conn = pyodbc.connect(odbc)
    print('Connected to external data warehouse')

    # imports the data into a pandas dataframe, and automatically closes the connection
    data = pd.read_sql(query, sql_conn)
    print('Executed query for crash events')

    return data


def get_non_crash_events():
    query = """With collision_events as 
    (
    Select Top {}
    sr.serial_number As smartrecorder_sn, 
    sr.application_version, 
    v.serial_number As vehicle_sn, 
    s.company_name, 
    s.site_level_five_name, 
    e.event_number, e.event_id,
    e.[file_name],
    LEFT(e.[file_name],len(e.[file_name])-4) as base_file_name,
    fel.captured_datetime, 
    tt.trigger_type_id, 
    tt.trigger_type_name, 
    o.observation_id, 
    o.observation_name, 
    o.category, 
    o.sub_category, 
    v.vehicle_id 
    From 
    sds_ExternalDataWarehouse.dbo.fact_Event_Lifecycle fel with (nolock)
    Join dbo.dim_SmartRecorder sr with (nolock) On fel.dim_smartrecorder_key = sr.dim_smartrecorder_key 
    Join dbo.dim_Trigger_Type tt with (nolock) On fel.dim_trigger_type_key = tt.dim_trigger_type_key 
    Join dbo.dim_Site s with (nolock) On sr.dim_sitehierarchy_key = s.dim_sitehierarchy_key 
    Join dbo.dim_Event e with (nolock) On fel.dim_event_key = e.dim_event_key 
    Join dbo.dim_Vehicle v with (nolock) On v.dim_vehicle_key = fel.dim_vehicle_key 
    Left Join dbo.fact_Event_Lifecycle_Observation felo with (nolock) On felo.dim_event_key = fel.dim_event_key 
    Left Join dbo.dim_Observation o with (nolock) On felo.dim_Observation_key = o.dim_observation_key 

    Where fel.captured_datetime < DATEADD(day,-14, GETDATE()) and fel.captured_datetime >= DATEADD(day,-28, GETDATE())
    and sr.application_version like '4%'
    and o.sub_category = 'Collision'

    order by newid()
    ),

    collision_with_row_id as 
    (
    Select 
    *
    ,ROW_NUMBER() OVER (Partition by trigger_type_id Order by trigger_type_id) as row_id
    From collision_events
    ),

    non_collision_events as 
    (
    Select Top {}
    sr.serial_number As smartrecorder_sn, 
    sr.application_version, 
    v.serial_number As vehicle_sn, 
    s.company_name, 
    s.site_level_five_name, 
    e.event_number, e.event_id,
    e.[file_name] as sde_file_name,
    LEFT(e.[file_name],len(e.[file_name])-4) as base_file_name,
    fel.captured_datetime, 
    tt.trigger_type_id, 
    tt.trigger_type_name, 
    o.observation_id, 
    o.observation_name, 
    o.category, 
    o.sub_category, 
    v.vehicle_id 
    From 
    sds_ExternalDataWarehouse.dbo.fact_Event_Lifecycle fel with (nolock)
    Join dbo.dim_SmartRecorder sr with (nolock) On fel.dim_smartrecorder_key = sr.dim_smartrecorder_key 
    Join dbo.dim_Trigger_Type tt with (nolock) On fel.dim_trigger_type_key = tt.dim_trigger_type_key 
    Join dbo.dim_Site s with (nolock) On sr.dim_sitehierarchy_key = s.dim_sitehierarchy_key 
    Join dbo.dim_Event e with (nolock) On fel.dim_event_key = e.dim_event_key 
    Join dbo.dim_Vehicle v with (nolock) On v.dim_vehicle_key = fel.dim_vehicle_key 
    Left Join dbo.fact_Event_Lifecycle_Observation felo with (nolock) On felo.dim_event_key = fel.dim_event_key 
    Left Join dbo.dim_Observation o with (nolock) On felo.dim_Observation_key = o.dim_observation_key 

    Where fel.captured_datetime < DATEADD(day,-14, GETDATE()) and fel.captured_datetime >= DATEADD(day,-28, GETDATE())
    and sr.application_version like '4%'
    and (o.sub_category != 'Collision' or o.sub_category IS NULL)
    and fel.dim_event_state_key = 4

    order by newid()
    )
    ,

    non_collision_with_row_id AS
    (
    Select 
    NCE.*
    ,ROW_NUMBER() OVER (Partition by trigger_type_id Order by trigger_type_id) as row_id
    From non_collision_events NCE
    )

    select 
    NCWRI.*
    from
    non_collision_with_row_id NCWRI
    Inner Join collision_with_row_id CWRI on NCWRI.trigger_type_id = CWRI.trigger_type_id AND NCWRI.row_id = CWRI.row_id
    order by newid()""".format(2*number_of_samples,number_of_samples*50)

    # specify the driver, server, and database
    driver = 'ODBC DRIVER 17 for SQL Server'
    server = 'db_externaldw.smartdrivesystems.com'
    database = 'sds_ExternalDataWarehouse'

    # choose a read-only node
    odbc = 'DRIVER={%s};SERVER=%s;DATABASE=%s;Trusted_Connection=Yes;ApplicationIntent=ReadOnly' % (
    driver, server, database)

    # connect to the database
    sql_conn = pyodbc.connect(odbc)
    print('Connected to external data warehouse')

    # imports the data into a pandas dataframe, and automatically closes the connection
    data = pd.read_sql(query, sql_conn)
    print('Executed query for non-crash events')

    return data


def get_non_crash_events_unbiased():
    query = """SELECT TOP ({}) dtt.trigger_type_name
            	, dtt.trigger_type_id
            	, elc.dim_captured_date_key
            	, evt.file_name_first_8 AS SR
            	, elc.dim_event_state_key
            	, evt.file_name AS sde_file_name
            
            FROM fact_Event_Lifecycle elc
            
            LEFT JOIN fact_Event_Lifecycle_Observation elco ON elco.dim_event_key = elc.dim_event_key
            	AND elco.dim_Observation_key NOT IN (18,19,20,21,22,47,127,128,129,130,131,132,133,200) -- grab everything NOT collision related
            
            INNER JOIN dim_Event evt ON evt.dim_event_key = elc.dim_event_key
            
            INNER JOIN dim_Trigger_Type dtt ON dtt.dim_trigger_type_key = elc.dim_trigger_type_key
            
            INNER JOIN dim_event_state es ON es.dim_event_state_key = elc.dim_event_state_key
            	AND es.dim_event_state_key IN (4)
            
            WHERE SUBSTRING(evt.file_name_first_8, 4, 1) = 'C'
            	AND elc.captured_datetime < DATEADD(day,-14, GETDATE())
            	AND elc.captured_datetime >= DATEADD(day,-28, GETDATE())""".format(2*number_of_samples)

    # specify the driver, server, and database
    driver = 'ODBC DRIVER 17 for SQL Server'
    server = 'db_externaldw.smartdrivesystems.com'
    database = 'sds_ExternalDataWarehouse'

    # choose a read-only node
    odbc = 'DRIVER={%s};SERVER=%s;DATABASE=%s;Trusted_Connection=Yes;ApplicationIntent=ReadOnly' % (
    driver, server, database)

    # connect to the database
    sql_conn = pyodbc.connect(odbc)
    print('Connected to external data warehouse')

    # imports the data into a pandas dataframe, and automatically closes the connection
    data = pd.read_sql(query, sql_conn)
    print('Executed query for crash events')

    return data


def get_non_crash_events_shock():
    query = """SELECT TOP ({}) dtt.trigger_type_name
            	, dtt.trigger_type_id
            	, elc.dim_captured_date_key
            	, evt.file_name_first_8 AS SR
            	, elc.dim_event_state_key
            	, evt.file_name AS sde_file_name
            
            FROM fact_Event_Lifecycle elc
            
            LEFT JOIN fact_Event_Lifecycle_Observation elco ON elco.dim_event_key = elc.dim_event_key
            	AND elco.dim_Observation_key NOT IN (18,19,20,21,22,47,127,128,129,130,131,132,133,200) -- grab everything NOT collision related
            
            INNER JOIN dim_Event evt ON evt.dim_event_key = elc.dim_event_key
            
            INNER JOIN dim_Trigger_Type dtt ON dtt.dim_trigger_type_key = elc.dim_trigger_type_key
                --AND dtt.trigger_type_id in (345, 365, 340, 341, 360, 361, 362)
                AND dtt.trigger_type_id in (340, 341, 360, 361, 362)
            
            INNER JOIN dim_event_state es ON es.dim_event_state_key = elc.dim_event_state_key
            	AND es.dim_event_state_key IN (4)
            
            WHERE SUBSTRING(evt.file_name_first_8, 4, 1) = 'C'
            	AND elc.captured_datetime < DATEADD(day,-14, GETDATE())
            	AND elc.captured_datetime >= DATEADD(day,-28, GETDATE())""".format(2*number_of_samples)

    # specify the driver, server, and database
    driver = 'ODBC DRIVER 17 for SQL Server'
    server = 'db_externaldw.smartdrivesystems.com'
    database = 'sds_ExternalDataWarehouse'

    # choose a read-only node
    odbc = 'DRIVER={%s};SERVER=%s;DATABASE=%s;Trusted_Connection=Yes;ApplicationIntent=ReadOnly' % (
    driver, server, database)

    # connect to the database
    sql_conn = pyodbc.connect(odbc)
    print('Connected to external data warehouse')

    # imports the data into a pandas dataframe, and automatically closes the connection
    data = pd.read_sql(query, sql_conn)
    print('Executed query for crash events')

    return data


def get_all_events():
    query = """
            SELECT dtt.trigger_type_name
            	, dtt.trigger_type_id
            	, elc.dim_captured_date_key
            	, evt.file_name AS sde_file_name
            
            FROM fact_Event_Lifecycle elc
            
            INNER JOIN dim_Event evt ON evt.dim_event_key = elc.dim_event_key
            
            INNER JOIN dim_Trigger_Type dtt ON dtt.dim_trigger_type_key = elc.dim_trigger_type_key
            
            WHERE SUBSTRING(evt.file_name_first_8, 4, 1) = 'C'
            	AND elc.captured_datetime < DATEADD(day,-14, GETDATE())
            	AND elc.captured_datetime >= DATEADD(day,-28, GETDATE())
            """

    # specify the driver, server, and database
    driver = 'ODBC DRIVER 17 for SQL Server'
    server = 'db_externaldw.smartdrivesystems.com'
    database = 'sds_ExternalDataWarehouse'

    # choose a read-only node
    odbc = 'DRIVER={%s};SERVER=%s;DATABASE=%s;Trusted_Connection=Yes;ApplicationIntent=ReadOnly' % (
    driver, server, database)

    # connect to the database
    sql_conn = pyodbc.connect(odbc)
    print('Connected to external data warehouse')

    # imports the data into a pandas dataframe, and automatically closes the connection
    data = pd.read_sql(query, sql_conn)
    print('Executed query for all events')

    return data




if __name__ == '__main__':
    
    if args.filelist:
        
        # may have to change this
        data_dir = r".\data"
    
        # create data directories if they do not exist
        try:
            Path(data_dir).mkdir(parents=True)
        except:
            pass
    
        # get list of SDE file names from querying external data warehouse
        df_crash = get_crash_events()
        df_all = get_all_events()
        
        # save dataframes to pickle
        df_crash.to_pickle(r".\data\crash_events.pkl")
        df_all.to_pickle(r".\data\all_events.pkl")
        

    
    


    if args.copyfiles:
        # may have to change this
        crash_events_dir = r".\data\crash_events"
        non_crash_events_dir = r".\data\non_crash_events"
    
        # create data directories if they do not exist
        try:
            Path(crash_events_dir).mkdir(parents=True)
            Path(non_crash_events_dir).mkdir(parents=True)
        except:
            pass
    
        # get list of SDE file names from querying external data warehouse
        df_crash = get_crash_events()
        #df_non_crash = get_non_crash_events_unbiased()
        df_non_crash = get_non_crash_events_shock()
        
        # save dataframes to pickle
        df_crash.to_pickle(r".\data\crash_data.pkl")
        df_non_crash.to_pickle(r".\data\non_crash_data.pkl")
    
        
        crash_filenames = df_crash['sde_file_name'].values.tolist()
        non_crash_filenames = df_non_crash['sde_file_name'].values.tolist()
        
        # copy over crash events
        collision_sde_count = 0
        for SDEname in crash_filenames:
            if collision_sde_count < number_of_samples:
                sr = SDEname[:8]
                src = os.path.join(r"\\Fas01-b\epp", sr, SDEname)
                dst = os.path.join(crash_events_dir, SDEname)
                try:
                    shutil.copyfile(src, dst)
                    collision_sde_count += 1
                except:
                    pass
            else:
                break
    
        print(f"Copied over {collision_sde_count} crash events")
    
    
        # copy over non-crash events
        non_collision_sde_count = 0
        for SDEname in non_crash_filenames:
            if non_collision_sde_count < number_of_samples:
                sr = SDEname[:8]
                src = os.path.join(r"\\Fas01-b\epp", sr, SDEname)
                dst = os.path.join(non_crash_events_dir, SDEname)
                try:
                    shutil.copyfile(src, dst)
                    non_collision_sde_count += 1
                except:
                    pass
            else:
                break
    
    
        print(f"Copied over {non_collision_sde_count} non-crash events")

