"""
This function inputs a pre-epp SDE file and 
returns a tuple of (ax,ay,ax) for the 5 Hz accelerometer

author: Andrew Kobach
"""

import tarfile
import struct
import pandas as pd
import argparse
import pprint
from tqdm import tqdm
import os
import pickle


def byte_to_number(b, i, n, strct, prec):
    # b is all the btyes in safety.bin
    # i is the starting byte index
    # n is the number of bytes after the starting byte
    # strct is how to do the converstion
    # prec is the precision for converting the int into a number
    return struct.unpack(strct, b[i:i+n])[0] / (2**prec)


def bit_to_number(b, i, ibit, n):
    # b is all the btyes in safety.bin
    # i is the byte index
    # ibit is the starting bit in the byte
    # n is the total number of bits
    
    a = bin(b[i])[2:].zfill(8) # strip off '0b' staring buffer
    ibit = -ibit + 8 - n # transform bit index to array index    
    return int(a[ibit:ibit+n],2)


def get_accel_signal(sde, mode="parse_highg"):
    
    # return dictionary
    data = {}
    
    # open SDE tarball
    SDEtar = tarfile.open(sde) 
    
    
    # get name for safety.bin, since can vary between firmware versions
    crashfilename = None
    for m in SDEtar.getmembers():
        if 'afety.' in m.name:
            safetyfilename = m.name
        
        if 'rash.' in m.name:
            crashfilename = m.name
            
    
    # extract bytes from Safety.BIN, or whatever it's called
    safetybin = SDEtar.extractfile(safetyfilename).read()
    
    # the number of bytes for each sample 
    duration = 20
    sampling_rate = 20
    sampling_byte_length = int(len(safetybin)/(duration*sampling_rate))
    
    
    # if sample byte length is not 114, abort
    data["corrupt"] = False
    if sampling_byte_length != 114:
        #print('ERROR: This SDE was produced with older firmware, or it is corrupt.')
        data["corrupt"] = True
        return data
        
        
        
    # low-g accelerometer data
    ax05 = [] # 0.5 Hz accelerometer
    ay05 = []
    az05 = []    
    ax5 = [] # 5 Hz accelerometer
    ay5 = []
    az5 = []  
    
    # speed data
    corrected_speed = []
    speed_flag = []
    
            
    # loop through each sample, where each sample has sampling_byte_length bytes
    for i in range(0, len(safetybin), sampling_byte_length): 
        
        k = i-1 # change index so the location of the bytes correspond to those in the documentation
        
        # ACCELEROMETERS
        ax05.append(byte_to_number(safetybin, k+1, 2, '<h', 12))
        ay05.append(byte_to_number(safetybin, k+3, 2, '<h', 12))
        az05.append(byte_to_number(safetybin, k+5, 2, '<h', 12))
        ax5.append(byte_to_number(safetybin, k+7, 2, '<h', 12))
        ay5.append(byte_to_number(safetybin, k+9, 2, '<h', 12))
        az5.append(byte_to_number(safetybin, k+11, 2, '<h', 12))
        
        # SPEED
        #corrected_speed.append(byte_to_number(safetybin, k+13, 2, '<h', 6))
        #speed_flag.append(bit_to_number(safetybin, k+15, 7, 1))
    
    
    
    # extract high-g accelerometer in crash.bin, if it exists
    ax_highg = []
    ay_highg = []
    az_highg = []
    if mode == "parse_highg":
        if crashfilename is not None:
            
            # extract bytes from Safety.BIN, or whatever it's called
            crashbin = SDEtar.extractfile(crashfilename).read()
        
            i = 0
            while i < len(crashbin):
                ax_highg.append(byte_to_number(crashbin, i, 2, '<h', 8))
                i += 2
                ay_highg.append(byte_to_number(crashbin, i, 2, '<h', 8))
                i += 2
                az_highg.append(byte_to_number(crashbin, i, 2, '<h', 8))
                i += 2
            
    '''
    # correct speed signal
    for i in range(len(corrected_speed)):
        if speed_flag[i] == 0:
            corrected_speed[i] = 0
    '''
    
    data["duration"] = 20
    data["length"] = len(ax05)
    data["0.5Hz_ax"] = ax05
    data["0.5Hz_ay"] = ay05
    data["0.5Hz_az"] = az05
    data["5Hz_ax"] = ax5
    data["5Hz_ay"] = ay5
    data["5Hz_az"] = az5
    data["ax_highg"] = ax_highg
    data["ay_highg"] = ay_highg
    data["az_highg"] = az_highg
    data["speed"] = corrected_speed
    
    
    return data




def get_accel_signal_fast(sde):
    
    # return dictionary
    data = {}
    
    # open SDE tarball
    SDEtar = tarfile.open(sde) 
    
    # extract bytes from Safety.BIN, or whatever it's called
    safetybin = SDEtar.extractfile("Safety.BIN").read()
    
    # the number of bytes for each sample 
    duration = 20
    sampling_rate = 20
    sampling_byte_length = int(len(safetybin)/(duration*sampling_rate))
    
    
    # if sample byte length is not 114, abort
    data["corrupt"] = False
    if sampling_byte_length != 114:
        #print('ERROR: This SDE was produced with older firmware, or it is corrupt.')
        data["corrupt"] = True
        return data
        
    # low-g accelerometer data
    ax5 = [] # 5 Hz accelerometer
    ay5 = []
    az5 = []  
            
    # loop through each sample, where each sample has sampling_byte_length bytes
    for i in range(0, len(safetybin), sampling_byte_length): 
        
        k = i-1 # change index so the location of the bytes correspond to those in the documentation
        
        # ACCELEROMETERS
        ax5.append(byte_to_number(safetybin, k+7, 2, '<h', 12))
        ay5.append(byte_to_number(safetybin, k+9, 2, '<h', 12))
        az5.append(byte_to_number(safetybin, k+11, 2, '<h', 12))
        
        
    data["duration"] = 20
    data["length"] = len(ax5)
    data["5Hz_ax"] = ax5
    data["5Hz_ay"] = ay5
    data["5Hz_az"] = az5
    
    
    return data







if __name__ == '__main__':

    # commmand line arguments
    parse = argparse.ArgumentParser()
    parse.add_argument("--n", type=int, default=100, help="Number of SDE files to parse")
    parse.add_argument("--parsefiles", action='store_true', default=False, help="Parse SDE files and save raw acceleration signals to file")
    args = parse.parse_args()
    
    if args.parsefiles:
        
        df_crash = pd.read_pickle(r".\data\crash_events.pkl")
        df_all = pd.read_pickle(r".\data\all_events.pkl")
            
        # shuffle
        df_crash = df_crash.sample(frac=1)
        df_all = df_all.sample(frac=1)
        
        # make list of dictionaries
        dict_crash = df_crash.to_dict('records') 
        dict_all = df_all.to_dict('records') 
        
        parsed_events_crash = []
        parsed_events_all = []
        
        nevents = 0
        for record in tqdm(dict_crash):
            sr = record['sde_file_name'][:8]
            sde_path = record['sde_file_name']
            src = os.path.join(r"\\Fas01-b\epp", sr, sde_path)
            
            try: # file might not exist
                data = get_accel_signal_fast(src)
            except:
                continue
            
            record['corrupt'] = False
            if data['corrupt']: # safety.bin might be corrupt
                record['corrupt'] = True
                continue
            record["duration"]  = data["duration"] 
            record["length"]  = data["length"] 
            record["5Hz_ax"] = data["5Hz_ax"] 
            record["5Hz_ay"] = data["5Hz_ay"]
            record["5Hz_az"]  = data["5Hz_az"] 
            nevents += 1
            if nevents >= args.n:
                break
            parsed_events_crash.append(record)
            
        
        nevents = 0
        for record in tqdm(dict_all):
            sr = record['sde_file_name'][:8]
            sde_path = record['sde_file_name']
            src = os.path.join(r"\\Fas01-b\epp", sr, sde_path)
            
            try: # file might not exist
                data = get_accel_signal_fast(src)
            except:
                continue
            
            record['corrupt'] = False
            if data['corrupt']: # safety.bin might be corrupt
                record['corrupt'] = True
                continue
            record["duration"]  = data["duration"] 
            record["length"]  = data["length"] 
            record["5Hz_ax"] = data["5Hz_ax"] 
            record["5Hz_ay"] = data["5Hz_ay"]
            record["5Hz_az"]  = data["5Hz_az"] 
            nevents += 1
            if nevents >= args.n:
                break
            parsed_events_all.append(record)
            
            
        
        # save list of dictionaries to pickle
        with open(r".\data\crash_events_with_acc.pkl", 'wb') as f:
            pickle.dump(parsed_events_crash, f)
            
        with open(r".\data\all_events_with_acc.pkl", 'wb') as f:
            pickle.dump(parsed_events_all, f)
        
        















    
    