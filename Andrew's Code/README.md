Starting from scratch:

First, execute two queries: 
(1) get all collisions from 14-28 days ago, and 
(2) get all events from 14-28 days ago. 
Save these queries to a pkl file in .\data.  
This can be done by executing the following script:

>> python GetSDEs.py --filelist

Second, pre-process the data, which untars each SDE in \\Fas01-b, parses Saftey.BIN, 
saves 5Hz accelerometer data to python dictionary, and saves everything to a pkl file in .\data
Remember to specify the number of events to parse, otherwise it will parse 100 files in each dataset by default:

>> python ParseAccelSignal.py --parsefiles --n 1000

Finally, run the script the reads in the accelerometer data and performs whatever analysis desired.
In Analyze.py, such an analysis using this data pipeline should be located under "flux":

>> python Analyze.py --flux 