import requests
import pandas as pd
import time


input_file = r'C:\Users\owens\OneDrive - Solera Holdings, Inc\Desktop\Owen\Bitbucket\event_ids.csv'
output_location = r'C:\Users\owens\OneDrive - Solera Holdings, Inc\Desktop\Owen\Bitbucket'
login_url = 'https://console.smartdrive.net/login'
request_url = 'https://console.smartdrive.net/SDEFiles/Metadata/N'

df = pd.read_csv(input_file)
df['event_id'] = df['event_id'].astype('str')
df['url'] = request_url+df['event_id']

payload = {'ctl00$ContentPlaceHolder1$company_name':'Smartdrive','ctl00$ContentPlaceHolder1$loginid':'AnalyticsDownload','ctl00$ContentPlaceHolder1$password':'VmUFTgeIyu19'}
session = requests.Session()
response = session.post(login_url, data = payload)


start = time.time()
for i in range(len(df['url'])):
    file_name = df['event_id'][i] + '.zip'
    print(file_name)
    request = session.get(df['url'][i])
    write_file =  open(output_location+'/'+file_name, "wb")
    write_file.write(request.content)
    write_file.close()
    time.sleep(3)


print(time.time() - start)