"""
This function inputs a pre-epp SDE file and 
returns a tuple of (ax,ay,ax) for the 5 Hz accelerometer

author: Andrew Kobach
"""

import tarfile
import struct


def byte_to_number(b, i, n, strct, prec):
    # b is all the btyes in safety.bin
    # i is the starting byte index
    # n is the number of bytes after the starting byte
    # strct is how to do the converstion
    # prec is the precision for converting the int into a number
    return struct.unpack(strct, b[i:i+n])[0] / (2**prec)


def get_accel_signal(sde):
    
    # open SDE tarball
    SDEtar = tarfile.open(sde) 
    
    
    # get name for safety.bin, since can vary between firmware versions
    for m in SDEtar.getmembers():
        if 'afety.' in m.name:
            safetyfilename = m.name
        
    
    # extract bytes from Safety.BIN, or whatever it's called
    safetybin = SDEtar.extractfile(safetyfilename).read()
    
    
    # the number of bytes for each sample 
    duration = 20
    sampling_rate = 20
    sampling_byte_length = int(len(safetybin)/(duration*sampling_rate))
    
    
    # if sample byte length is not 114, abort
    if sampling_byte_length != 114:
        #print('ERROR: This SDE was produced with older firmware, or it is corrupt.')
        exit(1)
        
    ax05 = [] # 0.5 Hz accelerometer
    ay05 = []
    az05 = []    
    ax5 = [] # 5 Hz accelerometer
    ay5 = []
    az5 = []  
    
    
    
    
    # loop through each sample, where each sample has sampling_byte_length bytes
    for i in range(0, len(safetybin), sampling_byte_length): 
        
        k = i-1 # change index so the location of the bytes correspond to those in the documentation
        
        # ACCELEROMETERS
        ax05.append(byte_to_number(safetybin, k+1, 2, '<h', 12))
        ay05.append(byte_to_number(safetybin, k+3, 2, '<h', 12))
        az05.append(byte_to_number(safetybin, k+5, 2, '<h', 12))
        ax5.append(byte_to_number(safetybin, k+7, 2, '<h', 12))
        ay5.append(byte_to_number(safetybin, k+9, 2, '<h', 12))
        az5.append(byte_to_number(safetybin, k+11, 2, '<h', 12))
    
    
    return (ax05, ay05, az05), (ax5, ay5, az5)
    
    